class Canvas {
	constructor() {
		this.button = document.getElementById('btn');
		this.cnv = document.getElementById('canvas');
		this.ctx = this.cnv.getContext('2d');
		this.startPosition = {};
		this.endPosition = {};
		this.lines = [];
		this.temporaryLine = {};
		this.dots = [];
		this.temporaryDots = [];
		this.isDrawStart = false;
		this.clicks = 0;
	}

	getCoordinates(e) {
		return { x: e.offsetX, y: e.offsetY };
	}

	drawLine() {
		this.ctx.beginPath();
		this.ctx.moveTo(this.startPosition.x, this.startPosition.y);
		this.ctx.lineTo(this.endPosition.x, this.endPosition.y);
		this.ctx.stroke();
	}

	drawDot() {
		let dotsToDraw = [...this.dots, ...this.temporaryDots];
		if (dotsToDraw.length !== 0) {
			dotsToDraw.map((dot) => {
				this.ctx.beginPath();
				this.ctx.arc(dot.x, dot.y, 4, 0, 2 * Math.PI, true);
				this.ctx.fillStyle = 'red';
				this.ctx.fill();
			});
		}
	}

	redrawStoredLines() {
		this.ctx.clearRect(0, 0, this.cnv.width, this.cnv.height);
		if (this.lines.length === 0) return;
		this.lines.map((line) => {
			this.ctx.beginPath();
			this.ctx.moveTo(line.start.x, line.start.y);
			this.ctx.lineTo(line.end.x, line.end.y);
			this.ctx.stroke();
		});
	}

	clearCanvas() {
		this.ctx.clearRect(0, 0, this.cnv.width, this.cnv.height);
		this.lines = [];
		this.dots = [];
		this.temporaryLine = {};
		this.temporaryDots = [];
		this.startPosition = {};
		this.endPosition = {};
		this.clicks = 0;
	}

	getIntersectionCoordinates(x1, y1, x2, y2, x3, y3, x4, y4) {
		if ((x1 === x2 && y1 === y2) || (x3 === x4 && y3 === y4)) {
			return false;
		}

		let denominator = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1);

		if (denominator === 0) {
			return false;
		}

		let ua = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / denominator;
		let ub = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / denominator;

		if (ua < 0 || ua > 1 || ub < 0 || ub > 1) {
			return false;
		}

		let x = x1 + ua * (x2 - x1);
		let y = y1 + ua * (y2 - y1);

		return { x, y };
	}

	checkIntersection(temporaryLine) {
		this.temporaryDots = [];
		if (temporaryLine) {
			for (let i = 0; i < this.lines.length; i++) {
				let intersectionCoordinates = this.getIntersectionCoordinates(
					this.lines[i].start.x,
					this.lines[i].start.y,
					this.lines[i].end.x,
					this.lines[i].end.y,
					this.temporaryLine.start.x,
					this.temporaryLine.start.y,
					this.temporaryLine.end.x,
					this.temporaryLine.end.y
				);
				if (Object.keys(intersectionCoordinates).length > 0) {
					this.temporaryDots.push(intersectionCoordinates);
				}
			}
		}
		for (let i = 0; i < this.lines.length; i++) {
			for (let j = 0; j < this.lines.length; j++) {
				let intersectionCoordinates = this.getIntersectionCoordinates(
					this.lines[i].start.x,
					this.lines[i].start.y,
					this.lines[i].end.x,
					this.lines[i].end.y,
					this.lines[j].start.x,
					this.lines[j].start.y,
					this.lines[j].end.x,
					this.lines[j].end.y
				);
				if (Object.keys(intersectionCoordinates).length > 0)
					this.dots.push(intersectionCoordinates);
			}
		}
	}

	mouseMoveHandler(e) {
		if (!this.isDrawStart) {
			return;
		} else {
			this.temporaryLine = { start: this.startPosition, end: this.endPosition };
			this.redrawStoredLines();
			this.endPosition = this.getCoordinates(e);
			this.drawLine();
			if (this.lines.length > 0) {
				this.checkIntersection(this.temporaryLine);
				this.drawDot();
			}
		}
	}

	clickHandler(e) {
		switch (e.button) {
			case 0:
				if (this.clicks === 0) {
					this.startPosition = this.getCoordinates(e);
					this.isDrawStart = true;
					this.clicks += 1;
				} else if (this.clicks !== 0) {
					this.lines.push(this.temporaryLine);
					this.dots.concat(this.temporaryDots);
					this.redrawStoredLines();
					this.drawDot();
					this.isDrawStart = false;
					this.startPosition = {};
					this.endPosition = {};
					this.temporaryDots = [];
					this.temporaryLine = {};
					this.clicks = 0;
				}
				break;
			case 2:
				this.isDrawStart = false;
				this.clicks = 0;
				this.startPosition = {};
				this.endPosition = {};
				this.temporaryDots = [];
				this.temporaryLine = {};
				break;
		}
	}
}

const canvas = new Canvas();

canvas.cnv.addEventListener('mousemove', canvas.mouseMoveHandler.bind(canvas));
canvas.cnv.addEventListener('touchmove', canvas.mouseMoveHandler.bind(canvas));
canvas.cnv.addEventListener('contextmenu', () => {
	canvas.startPosition = {};
	canvas.endPosition = {};
	canvas.clicks = 0;
});
canvas.cnv.addEventListener('click', (e) => canvas.clickHandler(e));

canvas.button.addEventListener('click', canvas.clearCanvas.bind(canvas));
